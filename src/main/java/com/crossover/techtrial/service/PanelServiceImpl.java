package com.crossover.techtrial.service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crossover.techtrial.dto.DailyElectricity;
import com.crossover.techtrial.model.HourlyElectricity;
import com.crossover.techtrial.model.Panel;
import com.crossover.techtrial.repository.PanelRepository;

/**
 * PanelServiceImpl for panel related handling.
 * 
 * @author Crossover
 *
 */
@Service
public class PanelServiceImpl implements PanelService
{

    @Autowired
    PanelRepository panelRepository;

    /*
     * (non-Javadoc)
     * 
     * @see com.crossover.techtrial.service.PanelService#register(com.crossover.techtrial .model.Panel)
     */

    @Override
    public void register(Panel panel)
    {
        panelRepository.save(panel);
    }

    public Panel findBySerial(String serial)
    {

        Panel panel = panelRepository.findFirstBySerial(serial);

        if (panel == null)
        {
            throw new RuntimeException("Panel serial number is not found in system:" + serial);
        }

        return panel;
    }

    @Override
    public List<DailyElectricity> getDailyElectricities(String panelSerial)
    {
        Panel panel = findBySerial(panelSerial);

        Map<Date, List<HourlyElectricity>> datewiseElectricities = organizeElectricities(panel);

        List<DailyElectricity> dailyElectricities = new ArrayList<>();

        datewiseElectricities.entrySet().forEach(e ->
        {
            if (e.getKey().before(DateUtils.truncate(new Date(), Calendar.DATE)))
            {
                Calendar cal = Calendar.getInstance();
                cal.setTime(e.getKey());
                LocalDate localDate = e.getKey().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                DailyElectricity dailyElectricity = new DailyElectricity();
                dailyElectricity.setDate(localDate);
                dailyElectricity.setMax(0l);
                dailyElectricity.setMin(Long.MAX_VALUE);
                dailyElectricity.setSum(0l);

                e.getValue().forEach(v ->
                {

                    if (v.getGeneratedElectricity() > dailyElectricity.getMax())
                    {
                        dailyElectricity.setMax(v.getGeneratedElectricity());
                    }
                    if (v.getGeneratedElectricity() < dailyElectricity.getMin())
                    {
                        dailyElectricity.setMin(v.getGeneratedElectricity());
                    }
                    dailyElectricity.setSum(dailyElectricity.getSum() + v.getGeneratedElectricity());
                });
                dailyElectricity.setAverage((double) dailyElectricity.getSum() / e.getValue().size());
                dailyElectricities.add(dailyElectricity);
            }

        });

        dailyElectricities.sort(Comparator.comparing(DailyElectricity::getDate));

        return dailyElectricities;

    }

    private Map<Date, List<HourlyElectricity>> organizeElectricities(Panel panel)
    {
        List<HourlyElectricity> hourlyElectricities = panel.getHourlyElectricities();

        Map<Date, List<HourlyElectricity>> datewiseElectricities = new HashMap<>();

        hourlyElectricities.forEach(e ->
        {
            Date date = DateUtils.truncate(Date.from(e.getReadingAt().atZone(ZoneId.systemDefault()).toInstant()), Calendar.DATE);

            if (datewiseElectricities.get(date) == null)
            {
                datewiseElectricities.put(date, new ArrayList<>());
            }

            datewiseElectricities.get(date).add(e);
        });

        return datewiseElectricities;
    }
}
