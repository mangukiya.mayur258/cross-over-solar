package com.crossover.techtrial.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.crossover.techtrial.model.HourlyElectricity;
import com.crossover.techtrial.model.Panel;
import com.crossover.techtrial.repository.HourlyElectricityRepository;

public class HourlyElectricityServiceImplTest
{
    @InjectMocks
    private HourlyElectricityServiceImpl classToTest;

    @Mock
    private HourlyElectricityRepository  hourlyElectricityRepository;

    @Mock
    private HourlyElectricity            hourlyElectricity;

    @Mock
    private Pageable                     pageable;

    @Mock
    private PanelService                 panelService;

    @Mock
    private Panel                        panel;

    @Mock
    private Page<HourlyElectricity>      hourlyElectricities;

    @Mock
    private List<HourlyElectricity>      hoList;

    @Before
    public void init()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSave()
    {
        String serial = "1231231231231234";
        Mockito.when(panelService.findBySerial(Mockito.anyString())).thenReturn(panel);
        Mockito.when(hourlyElectricityRepository.save(Mockito.any(HourlyElectricity.class))).thenReturn(hourlyElectricity);

        classToTest.save(hourlyElectricity, serial);

        Mockito.verify(panelService).findBySerial(Mockito.anyString());
        Assert.assertNotNull(panel);
    }

    @Test
    public void testGetAllHourlyElectricityByPanelId()
    {
        Mockito.when(hourlyElectricityRepository.findAllByPanelIdOrderByReadingAtDesc(Mockito.anyLong(), Mockito.any(Pageable.class)))
                .thenReturn(hourlyElectricities);
        classToTest.getAllHourlyElectricityByPanelId(new Long(1), pageable);

        Mockito.verify(hourlyElectricityRepository).findAllByPanelIdOrderByReadingAtDesc(Mockito.anyLong(), Mockito.any(Pageable.class));
        Assert.assertNotNull(hourlyElectricities);
    }

    @Test
    public void testFindByPanel()
    {
        Mockito.when(hourlyElectricityRepository.findOneByPanelId(Mockito.anyLong())).thenReturn(hoList);
        
        classToTest.findByPanel(panel.getId());
        
        Mockito.verify(hourlyElectricityRepository).findOneByPanelId(Mockito.anyLong());
        Assert.assertFalse(hoList.isEmpty());
    }
}
