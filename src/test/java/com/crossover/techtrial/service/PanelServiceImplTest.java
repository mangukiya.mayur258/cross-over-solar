package com.crossover.techtrial.service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.crossover.techtrial.model.HourlyElectricity;
import com.crossover.techtrial.model.Panel;
import com.crossover.techtrial.repository.PanelRepository;

public class PanelServiceImplTest
{
    @InjectMocks
    private PanelServiceImpl           classToTest;

    @Mock
    private PanelRepository            panelRepository;

    @Mock
    private Panel                      panel;

    @Mock
    Map<Date, List<HourlyElectricity>> datewiseElectricities = new HashMap<>();

    List<HourlyElectricity>            hourlyElectricities   = null;

    @Before
    public void init()
    {
        MockitoAnnotations.initMocks(this);

        hourlyElectricities = new ArrayList<>();
        HourlyElectricity hourlyElectricity = new HourlyElectricity();
        hourlyElectricity.setPanel(panel);
        hourlyElectricity.setReadingAt(LocalDateTime.of(2018, 8, 01, 00, 00));
        hourlyElectricity.setGeneratedElectricity(100l);
        hourlyElectricities.add(hourlyElectricity);
        Date date = DateUtils.truncate(Date.from(hourlyElectricity.getReadingAt().atZone(ZoneId.systemDefault()).toInstant()), Calendar.DATE);
        datewiseElectricities.put(date, hourlyElectricities);
    }

    @Test
    public void testRegister()
    {
        Mockito.when(panelRepository.save(Mockito.any(Panel.class))).thenReturn(panel);

        classToTest.register(panel);
        
        Mockito.verify(panelRepository).save(Mockito.any(Panel.class));
        Assert.assertNotNull(panel);
    }

    @Test
    public void testFindBySerial()
    {
        String serial = "SR23456455654987";
        Mockito.when(panelRepository.findFirstBySerial(Mockito.anyString())).thenReturn(panel);

        classToTest.findBySerial(serial);
        
        Mockito.verify(panelRepository).findFirstBySerial(Mockito.anyString());
        Assert.assertNotNull(panel);
        Assert.assertTrue(serial.length() == 16);
    }

    @Test
    public void getDailyElectricitiesTest()
    {
        Mockito.when(panel.getHourlyElectricities()).thenReturn(hourlyElectricities);
        String serial = "SR23456455654987";
        Mockito.when(panelRepository.findFirstBySerial(Mockito.anyString())).thenReturn(panel);
        
        classToTest.getDailyElectricities(serial);
        
        Mockito.verify(panel).getHourlyElectricities();
        Assert.assertNotNull(hourlyElectricities);
        Assert.assertNotNull(panel);
        Assert.assertTrue(serial.length() == 16);
    }
}
